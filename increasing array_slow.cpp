// My first idea, but...
// this one might work but we have a 1.00s time limit and its to slow
#include <iostream>

using namespace std;

const int amax = 2e5;
int len, arr[amax];
unsigned long long temp, turn = 0;

int main()
{
	cin >> len;
	for (int i = 0; i < len; i++)
		cin >> arr[i];

	while(true)
	{
		temp = turn;
		for (int i = 0; i < len; i++){
			if (arr[i + 1] < arr[i] && i + 1 < len)
			{
				arr[i + 1] = arr[i + 1] + 1;
				turn++;
				break;
			}
		}
		if (temp == turn)
			break;
	}
	cout << turn;
}
