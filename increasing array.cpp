#include <iostream>

using namespace std;


const int amax = 2e5;
int len, arr[amax], d = 0;
unsigned long long turn = 0;


int main()
{
	cin >> len;
	for (int i = 0; i < len; i++)
		cin >> arr[i];

	for (int i = 0; i < len; i++){
		if (arr[i + 1] < arr[i] && i + 1 < len)
		{
			d = arr[i] - arr[i + 1];
			turn = turn + d;
			arr[i + 1] = arr[i + 1] + d;
		}
	}
	cout << turn;
}